require 'test_helper'

class UserSignUpFlowTest < ActionDispatch::IntegrationTest
  
  test "Unregistered user should go to login page" do
    get "/"
    response.should redirect_to '/login'
  end
end
