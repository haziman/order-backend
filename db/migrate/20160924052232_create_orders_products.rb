class CreateOrdersProducts < ActiveRecord::Migration
  def change
    create_table :orders_products, :id => false do |t|
      t.references :order
      t.references :product
    end

    add_index :orders_products, [:order_id, :product_id],
      name: "orders_products_index",
      unique: true
  end
end
