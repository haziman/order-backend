Doorkeeper::Application.create!([
  {name: "ordering-backend", uid: "30d1ac94c72428b813f3930d9aea1af1744d8d3787de7b392603a51543decb54", secret: "1a7b2d39f2acd7949f1288b713c8e367d9624736bd7c5dcc8c7838ae557e1246", redirect_uri: "urn:ietf:wg:oauth:2.0:oob", scopes: "admin write"}
])
Product.create!([
  {name: "Pencil", description: "Sharp Pencil", unitprice: "1.2"},
  {name: "MacBook Pro 13\"", description: "Apple MacBook Pro 13 Inch", unitprice: nil}
])
Role.create!([
  {code: "adm", name: "Administrator", weight: 9999},
  {code: "cust", name: "Customer", weight: 3}
])
User.create!({
  username: "testuser", password: "testuser", email: "testing@orderingbackend.local", role_id: Role.find_by(code: "adm").id
})
