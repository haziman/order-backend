angular.module('niftymodal', [])
  .constant('modalEvent', 'modalEvent')
  .constant('modalErrorEvent', 'modalErrorEvent')
  .constant('modalSuccessEvent', 'modalSuccessEvent')
  .directive('modal', function(modalEvent, modalErrorEvent, modalSuccessEvent, $timeout){
    return {
      link: function(scope, el, attrs){
        var modalId = attrs.modalId;

        var currentLevel = undefined;

        scope.model = {};
        
        scope.$root.$on(modalSuccessEvent, function(event, data){
          scope.$apply(function(){
            scope.form_data.errors = null;
            scope.form_data.success = data;
            $timeout(function(){
              scope.successCallback()
              scope.close()
            }, 1000);
          });
        });
        
        scope.$root.$on(modalErrorEvent, function(event, data){
          scope.$apply(function(){
            scope.form_data.success = null;
            scope.form_data.errors = data;
          });
        });
        
        scope.$root.$on(modalEvent, function(event, data){                
          if(data.modalId === modalId){
            data.level = data.level || 'info';
            if(data.level){
              el.addClass(data.level);
              currentLevel = data.level;
            }else{
              el.removeClass(currentLevel);
              currentLevel = undefined;
            }

            scope.model.iconClass = data.iconClass || 'icon-remove-sign';
            scope.model.title = data.title;
            scope.model.modalUrl = data.modalUrl;
            scope.form_data = data.formData || {};
            scope.form_data.errors = null;
            scope.form_data.success = null;
            scope.successCallback = data.successCallback || scope.close
            scope.showClose = data.showClose || false;
            angular.extend(scope.model, data.scopes || {});

            if(data.show)
              el.addClass('md-show');
            else
              el.removeClass('md-show');
          }
        });

        scope.close = function(){
          scope.$root.$emit(modalEvent, {modalId: modalId});
        };
      }
    };
  });