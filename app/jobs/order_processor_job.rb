class OrderProcessorJob < ActiveJob::Base
  queue_as :default
  
#  rescue_from(Exception) do
#    retry_job wait: 5.second, queue: :default
#  end
  
  def perform(order)
    if order.process
      OrderMailer.notify_user_processed(order).deliver_later!
    end
  end
end
