class Role < ActiveRecord::Base
  audited
  has_many :users
end
