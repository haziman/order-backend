class Product < ActiveRecord::Base
  audited
	has_and_belongs_to_many :Orders
  
  validates :name, presence: true
end
