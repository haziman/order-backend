class Order < ActiveRecord::Base
  audited
	has_and_belongs_to_many :products
	belongs_to :user
	belongs_to :product
  
  validates :product_id, presence: true
  validates :user_id, presence: true
  validates :orderstatus_id, presence: true
  
  before_save do
    self.placed_at = Time.now
  end
  
  def process
    self.transaction do
      self.orderstatus_id = 2 # 2 for Completed, 1 for Pending
      unless self.save!
        return false
      end
      return true
    end
  end
end