//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require slimScroll/jquery.slimscroll
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
// 
// BOWER
//
//= require bootstrap/dist/js/bootstrap.min
//= require select2/select2
//= require angular/angular
//= require angular-route/angular-route
//= require angular-sanitize/angular-sanitize
//= require angular-cookies/angular-cookies
//= require angular-datatables/dist/angular-datatables
//= require angular-ui-select2/src/select2
//= require angular-slimscroll/angular-slimscroll
//
// END
//
//= require niftymodal
//= require_directory .