angular.module 'product-index'
  .controller 'ProductIndexCtrl', [
    "DTOptionsBuilder", "DTColumnBuilder",
    "OAuth", "$q", "Modal", "$compile", "$scope"
    (DTOptionsBuilder, DTColumnBuilder, OAuth, $q, Modal, $compile, $scope) ->
      vm = this;

      vm.model = gon.locales.models.product.label
      vm.dtInstance = {}
			
      vm.dtInstance = {}
      vm.dtOptions = DTOptionsBuilder
        .fromSource({
          url: '/api/v1/products.json',
          beforeSend: (xhr, settings) ->
            OAuth.withAccessToken (aToken) ->
              xhr.setRequestHeader('Authorization', aToken)
              return
            return
        })
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withDataProp('data')
        .withDisplayLength(100)
        .withPaginationType('full_numbers')
        .withOption('rowCallback', ( row, data, index ) ->
          view = "<a class='btn btn-default' ng-click='viewModal(record)'>View</a>"
          edit = "<a class='btn btn-default' ng-click='updateModal(record)'>Edit</a>"
          remove = "<a class='btn btn-default' ng-click='deleteModal(record)'>Remove</a>"
          scope = $scope.$new()
          scope.viewModal = vm.viewModal
          scope.updateModal = vm.updateModal
          scope.deleteModal = vm.deleteModal
          scope.record = data
          buttons = $compile(view.concat(edit).concat(remove))(scope)
          buttons.each(() ->
            angular.element('td:eq(3)', row).append(this) 
          )
        )
    
      vm.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID')
        DTColumnBuilder.newColumn('name').withTitle('Product Name').notSortable()
        DTColumnBuilder.newColumn('description').withTitle('Description').notSortable()
        DTColumnBuilder.newColumn('id').withTitle('Action').withOption('render', () -> "").notSortable()
      ]
  
      vm.submitAdd = (form_data) ->
        OAuth.apiCall({
          url: '/api/v1/products.json'
          method: 'POST',
          data: product: form_data
        }).then( (result) ->
          Modal.emit('modalSuccessEvent', result.status.message)
          return
        ).catch( (result) ->
          Modal.emit('modalErrorEvent', result.status.message)
          return
        )
        return

      vm.submitUpdate = (form_data) ->
        OAuth.apiCall({
          url: '/api/v1/products/'+form_data.id+'.json'
          method: 'PUT',
          data: product: form_data
        }).then( (result) ->
          Modal.emit('modalSuccessEvent', result.status.message)
          return
        ).catch( (result) ->
          Modal.emit('modalErrorEvent', result.status.message)
          return
        )
        return
      
      vm.submitDelete = (form_data) ->
        OAuth.apiCall({
          url: '/api/v1/products/'+form_data.id+'.json'
          method: 'DELETE'
        }).then( (result) ->
          Modal.emit('modalSuccessEvent', result.status.message)
          return
        ).catch( (result) ->
          Modal.emit('modalErrorEvent', result.status.message)
          return
        )
        return
      
      vm.addModal = () ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.order.add_new
          modalUrl: '/ng/views/products/newmodal'
          showClose: true
          successCallback: () ->
            vm.dtInstance.rerender()
            return
          scopes: {
            buttons: [
              { text: "Submit", btnClass: "btn-danger", action: vm.submitAdd }
            ]
            roleSelect2Options: vm.roleSelect2Options
          }
        }
        return

      vm.viewModal = (form_data) ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.order.view
          modalUrl: '/ng/views/products/viewmodal'
          showClose: true
          formData: form_data
          scopes: {
            buttons: []
          }
          roleSelect2Options: vm.roleSelect2Options
        }
        return
			
      vm.updateModal = (form_data) ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.order.update
          modalUrl: '/ng/views/products/editmodal'
          showClose: true
          formData: form_data
          successCallback: () ->
            vm.dtInstance.rerender()
            return
          scopes: {
            buttons: [
              { text: "Submit", btnClass: "btn-danger", action: vm.submitUpdate }
            ]
            roleSelect2Options: vm.roleSelect2Options
          }
        }
        return

      vm.deleteModal = (form_data) ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.order.delete
          modalUrl: '/ng/views/products/deletemodal'
          formData: form_data
          showClose: true
          successCallback: () ->
            vm.dtInstance.rerender()
            return
          scopes: {
            buttons: [
              { text: "Yes", btnClass: "btn-danger", action: vm.submitDelete }
            ]
            roleSelect2Options: vm.roleSelect2Options
          }
        }
        return
      return
	]