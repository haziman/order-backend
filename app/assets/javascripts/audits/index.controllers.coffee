angular.module 'audit-index'
  .controller 'AuditIndexCtrl', [
    "DTOptionsBuilder", "DTColumnBuilder",
    "OAuth", "$q", "Modal", "$compile", "$scope"
    (DTOptionsBuilder, DTColumnBuilder, OAuth, $q, Modal, $compile, $scope) ->
      vm = this;

      vm.model = gon.locales.models.audit.label
      
      vm.dtInstance = {}
      vm.dtOptions = DTOptionsBuilder
        .fromSource({
          url: '/api/v1/audits.json',
          beforeSend: (xhr, settings) ->
            OAuth.withAccessToken (aToken) ->
              xhr.setRequestHeader('Authorization', aToken)
              return
            return
        })
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withDataProp('data')
        .withDisplayLength(100)
        .withPaginationType('full_numbers')

      vm.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID').notSortable()
        DTColumnBuilder.newColumn('auditable').withTitle('Auditable').notSortable()
        DTColumnBuilder.newColumn('action').withTitle('Action').notSortable()
        DTColumnBuilder.newColumn('username').withTitle('User').notSortable()
        DTColumnBuilder.newColumn('ipaddress').withTitle('IP').notSortable()
        DTColumnBuilder.newColumn('auditable_id').withTitle('AuditableID').notSortable()
      ]
  
      return
	]