angular.module 'order-index'
  .controller 'OrderIndexCtrl', [
    "DTOptionsBuilder", "DTColumnBuilder",
    "OAuth", "$q", "Modal", "$compile", "$scope"
    (DTOptionsBuilder, DTColumnBuilder, OAuth, $q, Modal, $compile, $scope) ->
      vm = this;

      vm.model = gon.locales.models.order.label
      vm.dtInstance = {}
			
      vm.dtInstance = {}
      vm.dtOptions = DTOptionsBuilder
        .fromSource({
          url: '/api/v1/orders.json',
          beforeSend: (xhr, settings) ->
            OAuth.withAccessToken (aToken) ->
              xhr.setRequestHeader('Authorization', aToken)
              return
            return
        })
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withDataProp('data')
        .withDisplayLength(100)
        .withPaginationType('full_numbers')
        .withOption('rowCallback', ( row, data, index ) ->
          view = "<a class='btn btn-default' ng-click='viewModal(record)'>View</a>"
          edit = "<a class='btn btn-default' ng-click='updateModal(record)'>Edit</a>"
          remove = "<a class='btn btn-default' ng-click='deleteModal(record)'>Remove</a>"
          queue = "<a class='btn btn-default' ng-click='submitQueue(record)'>Queue</a>"
          scope = $scope.$new()
          scope.viewModal = vm.viewModal
          scope.updateModal = vm.updateModal
          scope.deleteModal = vm.deleteModal
          scope.record = data
          scope.submitQueue = vm.submitQueue
          buttons = $compile(view.concat(edit).concat(remove).concat(queue))(scope)
          buttons.each(() ->
            angular.element('td:eq(4)', row).append(this) 
          )
        )
    
      vm.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID')
        DTColumnBuilder.newColumn('product').withTitle('Product Name').notSortable()
        DTColumnBuilder.newColumn('user').withTitle('Ordered By').notSortable()
        DTColumnBuilder.newColumn('status').withTitle('Status').notSortable()
        DTColumnBuilder.newColumn('id').withTitle('Action').withOption('render', () -> "").notSortable()
      ]
  
      vm.submitQueue = (form_data) ->
        if confirm("Are you sure?")
          OAuth.apiCall({
            url: '/api/v1/orders/'+form_data.id+'/queue.json'
          }).then( (result) ->
            alert(result.status.message);
            return
          ).catch( (result) ->
            alert(result.status.message);
            return
          )
          return
        return
  
      vm.submitAdd = (form_data) ->
        OAuth.apiCall({
          url: '/api/v1/orders.json'
          method: 'POST',
          data: order: form_data
        }).then( (result) ->
          Modal.emit('modalSuccessEvent', result.status.message)
          return
        ).catch( (result) ->
          Modal.emit('modalErrorEvent', result.status.message)
          return
        )
        return

      vm.submitUpdate = (form_data) ->
        OAuth.apiCall({
          url: '/api/v1/orders/'+form_data.id+'.json'
          method: 'PUT',
          data: order: form_data
        }).then( (result) ->
          Modal.emit('modalSuccessEvent', result.status.message)
          return
        ).catch( (result) ->
          Modal.emit('modalErrorEvent', result.status.message)
          return
        )
        return
      
      vm.submitDelete = (form_data) ->
        OAuth.apiCall({
          url: '/api/v1/orders/'+form_data.id+'.json'
          method: 'DELETE'
        }).then( (result) ->
          Modal.emit('modalSuccessEvent', result.status.message)
          return
        ).catch( (result) ->
          Modal.emit('modalErrorEvent', result.status.message)
          return
        )
        return
      
      vm.addModal = () ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.order.add_new
          modalUrl: '/ng/views/orders/newmodal'
          showClose: true
          successCallback: () ->
            vm.dtInstance.rerender()
            return
          scopes: {
            buttons: [
              { text: "Submit", btnClass: "btn-danger", action: vm.submitAdd }
            ]
            roleSelect2Options: vm.roleSelect2Options
          }
        }
        return

      vm.viewModal = (form_data) ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.order.view
          modalUrl: '/ng/views/orders/viewmodal'
          showClose: true
          formData: form_data
          scopes: {
            buttons: []
          }
          roleSelect2Options: vm.roleSelect2Options
        }
        return
			
      vm.updateModal = (form_data) ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.order.update
          modalUrl: '/ng/views/orders/editmodal'
          showClose: true
          formData: form_data
          successCallback: () ->
            vm.dtInstance.rerender()
            return
          scopes: {
            buttons: [
              { text: "Submit", btnClass: "btn-danger", action: vm.submitUpdate }
            ]
            roleSelect2Options: vm.roleSelect2Options
          }
        }
        return

      vm.deleteModal = (form_data) ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.order.delete
          modalUrl: '/ng/views/orders/deletemodal'
          formData: form_data
          showClose: true
          successCallback: () ->
            vm.dtInstance.rerender()
            return
          scopes: {
            buttons: [
              { text: "Yes", btnClass: "btn-danger", action: vm.submitDelete }
            ]
            roleSelect2Options: vm.roleSelect2Options
          }
        }
        return
      return
	]