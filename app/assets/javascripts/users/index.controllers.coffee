angular.module 'user-index'
  .controller 'UserIndexCtrl', [
    "DTOptionsBuilder", "DTColumnBuilder",
    "OAuth", "$q", "Modal", "$compile", "$scope"
    (DTOptionsBuilder, DTColumnBuilder, OAuth, $q, Modal, $compile, $scope) ->
      vm = this;

      vm.model = gon.locales.models.user.label
      vm.roleSelect2Options = query: (query) ->
        data = results: gon.lookups.roles
        query.callback data
        return
      vm.dtInstance = {}
			
      vm.dtInstance = {}
      vm.dtOptions = DTOptionsBuilder
        .fromSource({
          url: '/api/v1/users.json',
          beforeSend: (xhr, settings) ->
            OAuth.withAccessToken (aToken) ->
              xhr.setRequestHeader('Authorization', aToken)
              return
            return
        })
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withDataProp('data')
        .withDisplayLength(100)
        .withPaginationType('full_numbers')
        .withOption('rowCallback', ( row, data, index ) ->
          view = "<a class='btn btn-default' ng-click='viewUserModal(record)'>View</a>"
          edit = "<a class='btn btn-default' ng-click='updateUserModal(record)'>Edit</a>"
          remove = "<a class='btn btn-default' ng-click='deleteUserModal(record)'>Remove</a>"
          scope = $scope.$new()
          scope.viewUserModal = vm.viewUserModal
          scope.updateUserModal = vm.updateUserModal
          scope.deleteUserModal = vm.deleteUserModal
          scope.record = data
          buttons = $compile(view.concat(edit).concat(remove))(scope)
          buttons.each(() ->
            angular.element('td:eq(5)', row).append(this) 
          )
        )
    
      vm.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID')
        DTColumnBuilder.newColumn('firstname').withTitle('First name').notSortable()
        DTColumnBuilder.newColumn('lastname').withTitle('Last name').notSortable()
        DTColumnBuilder.newColumn('email').withTitle('Email').notSortable()
        DTColumnBuilder.newColumn('role').withTitle('Role').notSortable()
        DTColumnBuilder.newColumn('id').withTitle('Action').withOption('render', () -> "").notSortable()
      ]
  
      vm.submitAddUser = (form_data) ->
        OAuth.apiCall({
          url: '/api/v1/users.json'
          method: 'POST',
          data: user: form_data
        }).then( (result) ->
          Modal.emit('modalSuccessEvent', result.status.message)
          return
        ).catch( (result) ->
          Modal.emit('modalErrorEvent', result.status.message)
          return
        )
        return

      vm.submitUpdateUser = (form_data) ->
        OAuth.apiCall({
          url: '/api/v1/users/'+form_data.id+'.json'
          method: 'PUT',
          data: user: form_data
        }).then( (result) ->
          Modal.emit('modalSuccessEvent', result.status.message)
          return
        ).catch( (result) ->
          Modal.emit('modalErrorEvent', result.status.message)
          return
        )
        return
      
      vm.submitDeleteUser = (form_data) ->
        OAuth.apiCall({
          url: '/api/v1/users/'+form_data.id+'.json'
          method: 'DELETE'
        }).then( (result) ->
          Modal.emit('modalSuccessEvent', result.status.message)
          return
        ).catch( (result) ->
          Modal.emit('modalErrorEvent', result.status.message)
          return
        )
        return
      
      vm.addUserModal = () ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.user.add_new_user
          modalUrl: '/ng/views/users/newusermodal'
          showClose: true
          successCallback: () ->
            vm.dtInstance.rerender()
            return
          scopes: {
            buttons: [
              { text: "Submit", btnClass: "btn-danger", action: vm.submitAddUser }
            ]
            roleSelect2Options: vm.roleSelect2Options
          }
        }
        return

      vm.viewUserModal = (form_data) ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.user.view_user
          modalUrl: '/ng/views/users/viewusermodal'
          showClose: true
          formData: form_data
          scopes: {
            buttons: []
          }
          roleSelect2Options: vm.roleSelect2Options
        }
        return
			
      vm.updateUserModal = (form_data) ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.user.update_user
          modalUrl: '/ng/views/users/editusermodal'
          showClose: true
          formData: form_data
          successCallback: () ->
            vm.dtInstance.rerender()
            return
          scopes: {
            buttons: [
              { text: "Submit", btnClass: "btn-danger", action: vm.submitUpdateUser }
            ]
            roleSelect2Options: vm.roleSelect2Options
          }
        }
        return

      vm.deleteUserModal = (form_data) ->
        Modal.show {
          show: true
          modalId: 'md-effect-1'
          title: gon.locales.models.user.delete_user
          modalUrl: '/ng/views/users/deleteusermodal'
          formData: form_data
          showClose: true
          successCallback: () ->
            vm.dtInstance.rerender()
            return
          scopes: {
            buttons: [
              { text: "Yes", btnClass: "btn-danger", action: vm.submitDeleteUser }
            ]
            roleSelect2Options: vm.roleSelect2Options
          }
        }
        return
      return
	]