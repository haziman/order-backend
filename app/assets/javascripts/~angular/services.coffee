angular.module "OrderingBackend"
  .service "OAuth", ["$http", "$cookies", "$filter",($http, $cookies, $filter) ->
    dataStore = {}
    withAccessToken = (cb) ->
      now = new Date().getTime().toString()
      now = now.substring(0, now.length - 3)
      now = parseInt(now)
      dataStore["accessToken"] = dataStore["accessToken"] || $cookies.getObject("accessToken")
      aToken = dataStore["accessToken"]
      if aToken && aToken["token"] != ""
        expire_on = parseInt(aToken["created_at"]) + parseInt(aToken["expires_in"])
        if now < expire_on
          aToken = $filter("titleCase")(aToken["token_type"]) + " " + aToken["access_token"]
          cb(aToken)
          return
      $http({
        method: 'post'
        url: '/auth/token'
        data: {
          client_id: '30d1ac94c72428b813f3930d9aea1af1744d8d3787de7b392603a51543decb54'
          client_secret: '1a7b2d39f2acd7949f1288b713c8e367d9624736bd7c5dcc8c7838ae557e1246'
          redirect_uri: 'urn:ietf:wg:oauth:2.0:oob'
          grant_type: 'client_credentials'
        }
      }).success((result) ->
        $cookies.putObject("accessToken", aToken = dataStore["accessToken"] = result)
        aToken = $filter("titleCase")(aToken["token_type"]) + " " + aToken["access_token"]
        cb(aToken)
        return
      ).error((result) -> 
        console.log(result)
        return
      )
      return
    
    {
      withAccessToken: withAccessToken
      apiCall: (options) ->
        if typeof(options) == "string"
          options = {
            url: options
            method: 'get'
            headers: {}
          }
        else
        return new Promise (resolve, reject) ->
          withAccessToken (aToken) ->
            options["headers"] = options["headers"] || {}
            options["headers"]["Authorization"] = aToken
            $http(options).success(resolve).error(reject)
            return
          return
        return
    }
  ]
  .service "Modal", [
    "$rootScope", 
    ($rootScope) ->
      showModal = (opts) ->
        $rootScope.$emit 'modalEvent', opts
        return
      
      emitModal = (type, data) -> 
        $rootScope.$emit type, data
        return
        
      { show: showModal, emit: emitModal }
  ]
