angular
  .module "OrderingBackend"
  .filter "titleCase", () ->
    (input) ->
      return input.replace /\w\S*/g, (txt) -> 
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  .filter "toString", () ->
    (input) ->
      return "".concat(input)