angular
  .module "OrderingBackend", [
    "ngRoute", "ngSanitize","ngCookies", "datatables", "niftymodal", "ui.select2",
    "user-index", "order-index", "product-index", "audit-index"
  ]
  .config [
    "$routeProvider",
    ($routeProvider) ->
      $routeProvider.when('/users',
        templateUrl: '/ng/views/users/index.html'
        controller: 'UserIndexCtrl as vm'
      ).when('/orders',
        templateUrl: '/ng/views/orders/index.html'
        controller: 'OrderIndexCtrl as vm'
      ).when('/products',
        templateUrl: '/ng/views/products/index.html'
        controller: 'ProductIndexCtrl as vm'
      ).when('/audits',
        templateUrl: '/ng/views/audits/index.html'
        controller: 'AuditIndexCtrl as vm'
      ).otherwise '/users'
      return
  ]