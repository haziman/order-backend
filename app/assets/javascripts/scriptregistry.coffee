ScriptRegistry = ->
  scripts = {}
  {
    execute: (page) ->
      if !gon.params.page.match(page)
        return
      if !scripts[page]
        return
      scripts[page].forEach (script) -> 
        script.call this
      return
    register: (page, cb) ->
      scripts[page] = scripts[page] || new Array()
      scripts[page].push(cb)
      return

  }

window.ScriptRegistry = new ScriptRegistry()