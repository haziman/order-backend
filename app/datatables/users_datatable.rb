class UsersDatatable < AjaxDatatablesRails::Base

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= ["User.id"]
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= ["User.firstname"]
  end

  private

  def data
    records.map do |record| {
      id: record.id,
      firstname: record.firstname,
      lastname: record.lastname,
      email: record.email,
      role: record.role.name,
      username: record.username,
      role_id: record.role_id.to_s
    } end
  end

  def get_raw_records
    User.joins(:role)
  end

  # ==== Insert 'presenter'-like methods below if necessary
end
