class OrdersDatatable < AjaxDatatablesRails::Base
  attr_reader :orderstats
  
  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= ['Order.id']
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= []
  end

  private

  def data
    records.map do |record| {
      id: record.id,
      user: record.user.fullname,
      product: record.product.name,
      status: get_orderstat(record.orderstatus_id),
      user_id: record.user_id.to_s,
      product_id: record.product_id.to_s,
      orderstatus_id: record.orderstatus_id.to_s
    } end
  end

  def get_raw_records
    # insert query here
    Order
    .joins(:user)
    .joins(:product)
  end

  private
  def get_orderstat(id)
    case id
      when 1;
        "Pending"
      when 2;
        "Completed"
    end
  end
  # ==== Insert 'presenter'-like methods below if necessary
end
