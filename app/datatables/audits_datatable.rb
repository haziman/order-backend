class AuditsDatatable < AjaxDatatablesRails::Base

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= ['Audited::Audit.id']
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= []
  end

  private

  def data
    records.map do |record| {
      id: record.id,
      auditable: record.auditable_type,
      action: record.action,
      username: record.user.try(:username),
      ipaddress: record.remote_address,
      auditable_id: record.auditable_id,
    } end
  end

  def get_raw_records
    Audited::Audit.all
  end

  # ==== Insert 'presenter'-like methods below if necessary
end
