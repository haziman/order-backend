class OrderMailer < ApplicationMailer
  default from: "no-reply@orderingbackend.local"
  
  def notify_user_processed order
    @order = order
    mail( to: order.user.email, subject: "Order ##{order.id} Processed"  ) do |f|
      f.text
      f.html
    end
  end
end
