class Api::ApiController < ActionController::Base
	before_action :doorkeeper_authorize!
	respond_to :json
  
  before_action do 
    @status = { :code => :success }
  end
  
  rescue_from Exception do |exception|
    exname = exception.class.name.underscore.gsub /\//, '-' 
    puts exception.inspect
    @status[:code] = :error
    @status[:message] = begin
      I18n.t("exception.messages.#{exname}", raise: true)
    rescue
      "#{exception.message}"
    end
  
    render status: 500, json: @status
  end

end