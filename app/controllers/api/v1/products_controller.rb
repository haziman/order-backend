module Api
	class V1::ProductsController < ApiController

		def index
      respond_with ProductsDatatable.new(view_context)
		end
    
    def create
      @product = Product.new( product_params )
      @product.save!
      @status[:message] = I18n.t "products$create.messages.create_success"
    end
    
    def update
      @product = Product.find product_params.delete(:id)
      @product.update_attributes!( product_params )
      @status[:message] = I18n.t "products$update.messages.update_success"
    end
    
    def destroy
      product_id = params.require(:id)
      @product = Product.find product_id
      @product.destroy!
      @status[:message] = I18n.t "products$destroy.messages.destroy_success"
    end
    
    protected
      def product_params
        case params[:action].to_sym
        when :create, :update, :destroy
          params.require(:product).permit(:id, :name, :description)
        end
      end
	end
end