module Api
	class V1::UsersController < ApiController

		def index
      respond_with UsersDatatable.new(view_context, User.all)
		end
    
    def create
      @user = User.new( user_params )
      @user.save!
      @status[:message] = I18n.t "users$create.messages.user_create_success"
    end
    
    def update
      @user = User.find user_params.delete(:id)
      user_params.delete(:password) if user_params[:password].nil?
      @user.update_attributes!( user_params )
      @status[:message] = I18n.t "users$update.messages.user_update_success"
    end
    
    def destroy
      user_id = params.require(:id)
      @user = User.find user_id
      @user.destroy!
      @status[:message] = I18n.t "users$destroy.messages.user_destroy_success"
    end
    
    protected
      def user_params
        case params[:action].to_sym
        when :create, :update, :destroy
          params.require(:user).permit(:id, :firstname, :email, :lastname, :username, :password, :role_id)
        end
      end
	end
end