module Api
	class V1::OrdersController < ApiController

		def index
      respond_with OrdersDatatable.new(view_context)
		end
    
    def create
      @order = Order.new( order_params )
      @order.save!
      delay = rand(5..30)
      OrderProcessorJob.set( wait: delay.seconds ).perform_later @order
      @status[:message] = I18n.t("orders$create.messages.create_success")
        .concat(" and ")
        .concat(I18n.t("orders$create.messages.queued_success"))
        .concat(" for #{delay} seconds")
    end
    
    def update
      @order = Order.find order_params.delete(:id)
      @order.update_attributes!( order_params )
      @status[:message] = I18n.t "orders$update.messages.update_success"
    end
    
    def destroy
      order_id = params.require(:id)
      @order = Order.find order_id
      @order.destroy!
      @status[:message] = I18n.t "orders$destroy.messages.destroy_success"
    end
    
    def queue
      @order = Order.find params.delete(:order_id)
      delay = rand(5..30)
      OrderProcessorJob.set( wait: delay.seconds ).perform_later @order
      @status[:message] = "Order ".concat(I18n.t("orders$create.messages.queued_success"))
        .concat(" for #{delay} seconds")
    end
    
    protected
      def order_params
        case params[:action].to_sym
        when :create, :update, :destroy
          params.require(:order).permit(:id, :user_id, :product_id, :orderstatus_id)
        end
      end
	end
end