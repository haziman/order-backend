class AuditsController < AdministrationController
  def index
  {}
  end
  
  def render_opts action
    { 
      partial: "audits/#{action}", layout: false, locals: send("#{params[:railsact]}")
    }
  end
end