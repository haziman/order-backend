class AdministrationController < ApplicationController
	layout 'administration'
	
	# Redirect to user login by default
	before_action :authenticate_user!
	
	# To facilitate selective pipeline asset execution
	def render *args
		gon.params = {
			page: "#{params[:controller]}##{params[:action]}",
			format: request.format.symbol
		}
		gon.locales = I18n.t('.')
		super
	end
end