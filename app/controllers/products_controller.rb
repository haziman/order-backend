class ProductsController < AdministrationController
  
  def index; end
  
  def render_opts action
    { 
      partial: "products/#{action}", layout: false, locals: send("#{params[:railsact]}")
    }
  end
  
  private
  def deletemodal
  end
  def newmodal 
    {}
  end
  alias_method :editmodal, :newmodal
  alias_method :viewmodal, :newmodal
end
