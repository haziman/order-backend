class Ng::ViewsController < ActionController::Base
  before_action do
    @page_heading = [ params[:railsctl],params[:railsact] ].join("$").concat(".page_heading")
  end
  
  def show
    railsctl = "#{params[:railsctl].titlecase}Controller".constantize.new
    railsctl.request = request
    railsctl.response = response
    render railsctl.render_opts(params[:railsact])
  end
end