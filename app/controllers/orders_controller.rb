class OrdersController < AdministrationController
  def index; end
  
  def render_opts action
    { 
      partial: "orders/#{action}", layout: false, locals: send("#{params[:railsact]}")
    }
  end
  
  private
  def deletemodal
  end
  def newmodal 
    return { 
      products: Product.all, users: User.all, orderstatuses: [
        { id: 1, name: "Pending" },
        { id: 2, name: "Completed" },
      ]
    }
  end
  alias_method :editmodal, :newmodal
  alias_method :viewmodal, :newmodal
end