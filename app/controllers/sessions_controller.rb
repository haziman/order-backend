class SessionsController < Devise::SessionsController
	skip_before_action :authenticate_user!, only: [:new, :create]
	
	def new
		super
	end
	
	def create
		super
	end
	
	before_action :configure_permitted_parameters
	private
	
		def configure_permitted_parameters
			devise_parameter_sanitizer.permit(:sign_in, keys: [
				:login, :password
			])
		end
end
