class UsersController < AdministrationController
  
  def index; end
  
  def render_opts action
    { 
      partial: "users/#{action}", layout: false, locals: {
        roles: Role.all  
      }
    }
  end
end
