class HomeController < AdministrationController
  def index
    gon.lookups = {}
    gon.lookups["roles"] = Role.all.map do |role| {
      id: role.id,
      text: role.name
    } end
  end
end
