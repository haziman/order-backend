class RegistrationsController < Devise::RegistrationsController
	skip_before_action :authenticate_user!, only: [:new, :create]
	
  def new
		super
  end
	
	before_action :configure_permitted_parameters
	protected
		def configure_permitted_parameters
			devise_parameter_sanitizer.permit(:sign_up, keys: [
				:firstname, :lastname, :username, :email, :password, :role_id
			])
		end
end
