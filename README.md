# README #

# Here's what I have already done #

1. User authentication using Devise
2. OAuth2 API endpoints using Doorkeeper
3. CRUD implementation using Angular
4. Implemented ActiveJob using Sidekiq as Queueing Backend
5. Implemented ActionMailer that sends to [MailCatcher Gem](http://mailcatcher.me)

# Issues #

Currently i'm struggling with an interal bug of ajax-datatables-rails gem which stalled the development.
As I am more concerned that this bug also might affect my current project, i'm going to find the solution first, leaving this project temporarily.

Update (26/09/2016): I have fixed the bug by overriding the gem class within my app initializer

No other significant issue right now. Proceeding to implementing integration test

## Steps to run the app ##

### Requirements ###
My environment for project is:

*Ruby v2.3.1*,
*Rails v4.2.7.1*,
*Redis v3.2.3*,
*SQLite 3*

1. `git clone https://bitbucket.org/haziman/order-backend.git` 
[Screen](https://drive.google.com/open?id=0B9h4Vn0gcGVKcDJoUlRINzR4cFE)
2. `cd order-backend`
3. `bundle install` \(make sure you have ruby and bundler gem\) 
[Screen](https://drive.google.com/open?id=0B9h4Vn0gcGVKTlBMemMwdUdFUUk)
4. `rake db:drop`
5. `rake db:create`
6. `rake db:migrate`
7. `rake db:seed` 
[Screen1](https://drive.google.com/open?id=0B9h4Vn0gcGVKZnRObnVOYWo2Vlk),
[Screen2](https://drive.google.com/open?id=0B9h4Vn0gcGVKZGNoaGh3QmVreXM)
8. You need to have a Redis server running in background at port 6379 to use ActiveJob
8. `foreman start` 
[Screen](https://drive.google.com/open?id=0B9h4Vn0gcGVKZF9aWjZ5elJQRmc),
9. Browse `http://localhost:3000/` and sign up to get in 
[Screen](https://drive.google.com/open?id=0B9h4Vn0gcGVKVWpKYTdfbW40Q28)
[Sample Crud Listing](https://drive.google.com/open?id=0B9h4Vn0gcGVKYzI1aUpraWl4RDA)
[Crud Create](https://drive.google.com/open?id=0B9h4Vn0gcGVKNXU2SUREWUhmVG8)
[Order - ActiveJob](https://drive.google.com/open?id=0B9h4Vn0gcGVKV3ZsR1FUSHptb1k)
10. Browse `http://localhost:3000/admin/queues` to view sidekiq dashboard 
[Screen](https://drive.google.com/open?id=0B9h4Vn0gcGVKcjczSE1SU2g2Zk0)
11. Browse `http://localhost:1080/` to view smtp mailbox 
[Screen](https://drive.google.com/open?id=0B9h4Vn0gcGVKNm1SRXZjY3c0cTQ)

## Steps to run the test ##

1. Make sure you can run the app first, then
2. `cd order-backend`
3. `RAILS_ENV=test rake db:drop`
4. `RAILS_ENV=test rake db:create`
5. `RAILS_ENV=test rake db:migrate`
6. `rspec --format documentation`
[Screen 1](https://drive.google.com/open?id=0B9h4Vn0gcGVKOE9lREFSelFKcGc)
[Screen 2](https://drive.google.com/open?id=0B9h4Vn0gcGVKX2U1S25jQzBvdUE)

## Source Code Structure ##

Structure of my app is mostly similar to standard rails app. However, there are some difference:

1. Controllers:
   - APIs are served under Api::V1 namespace. So every endpoints is prefixed with /api/v1/. 
   - Page controllers inherits AdministrationController, which then inherits ApplicationController

2. Assets:
   - Pages of logged-in users uses administration.js and administration.css as their assets
   - Angular scripts are loaded from administration.js
   - Page specific scripts are handled by custom js (scriptregistry.coffee), but not currently used as AngularJS did the same thing
