AjaxDatatablesRails.configure do |config|
  # available options for db_adapter are: :oracle, :pg, :mysql2, :sqlite3
  config.db_adapter = :sqlite3

  # available options for paginator are: :simple_paginator, :kaminari, :will_paginate
  config.paginator = :simple_paginator
end

class AjaxDatatablesRails::Base
  
  def new_sort_column(item)
    model, column = sortable_columns[item[:column].to_i].split('.')
    col = [model.constantize.table_name, column].join('.')
  end
  
  def new_search_condition(column, value)
    model, column = column.split('.')
    model = model.constantize
    casted_column = ::Arel::Nodes::NamedFunction.new('CAST', [model.arel_table[column.to_sym].as(typecast)])
    casted_column.matches("%#{sanitize_sql_like(value)}%")
  end
end