require 'sidekiq/web'

Rails.application.routes.draw do
	use_doorkeeper :scope => "auth"
  mount Sidekiq::Web, at: '/admin/queues'
  
	devise_for :users, controllers: {
		sessions: "sessions",
		registrations: "registrations"
	}, path: "", :path_names => {
		sign_in: "login", 
		sign_out: "logout"
	}
  
  namespace "ng" do
    get "views/:railsctl/:railsact" => "views#show"
  end
	
	namespace :api do
		namespace :v1 do
			match 'me' => 'api#me', via: [:get, :post]
			resources :users
      resources :orders do 
        get 'queue' => 'orders#queue'
      end
      resources :products
      resources :audits
		end
	end
	
	root to: 'home#index'
end
