require 'spec_helper'

describe "Product CRUD" do
  
  def visit_put_api(data = {}, enable_token = true, method = :put)
    if enable_token
      page.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
    else
      page.driver.header('Authorization', '')
    end
    page.driver.submit method, "/api/v1/products/#{data[:product][:id]}.json", data
  end
  
  def visit_post_api(data = {}, enable_token = true)
    if enable_token
      page.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
    else
      page.driver.header('Authorization', '')
    end
    page.driver.submit :post, "/api/v1/products.json", data
  end
  
  describe "GET ng/views/products/index" do
    it "renders the page" do
      visit ng_path("products", "index")
      expect(page.status_code).to eq 200
      expect(page).to have_css('h1', text: I18n.t('products$index.page_heading'))
    end
  end
  
  describe "GET api/v1/products.json" do
    def visit_api
      page.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
      visit '/api/v1/products.json'
    end
    
    it "should not be able to get data without access_token" do
      visit api_v1_products_path
      expect(page.status_code).to eq 401
    end
    
    it "should be able to get data with access_token" do
      visit_api
      expect(page.status_code).to eq 200
    end
    
    it "should return JSON" do
      visit_api
      expect(page.response_headers['Content-Type']).to match /^application\/json/
    end
    
    it "should return JSON understandable by DataTables" do
      visit_api
      parsed = JSON.parse(page.body)
      expect(parsed).to include('draw')
      expect(parsed).to include('data')
    end
  end
  
  describe "POST /api/v1/products.json" do
    it "should not be able to post data without access_token" do
      visit_post_api({product: {id: 1}}, false)
      expect(page.status_code).to eq 401
    end
    
    it "should not be able to post invalid data with access_token" do
      visit_post_api({
        product: { 
          description: "Test product description"
        }
      })
      expect(page.status_code).to eq 500
    end
    
    it "should be able to post data with access_token and receive JSON" do
      visit_post_api({
        product: { 
          name: "Test Product",
          description: "Test product description"
        }
      })
      expect(page.status_code).to eq 200
      expect(page.response_headers['Content-Type']).to match /^application\/json/
      parsed = JSON.parse(page.body)
      expect(parsed["status"]["code"]).to eq "success"
    end
  end
  
  describe "PUT /api/v1/products.json" do
    
    it "should not be able to update user without access_token" do
      visit_put_api({product: {id: 1}}, false)
      expect(page.status_code).to eq 401
    end
    
    it "should not be able to update invalid data with access_token" do
      visit_post_api({
        product: { 
          name: "Test Product",
          description: "Test product description"
        }
      })
      expect(page.status_code).to eq 200
      visit_put_api({
        product: { 
          id: 2,
          name: nil
        }
      })
      expect(page.status_code).to eq 500
    end
    
    it "should be able to update data with access_token and receive JSON" do
      visit_post_api({
        product: { 
          name: "Test Product",
          description: "Test product description"
        }
      })
      expect(page.status_code).to eq 200
      visit_put_api({
        product: { 
          id: 2,
          name: "Test Product Changed"
        }
      })
      expect(page.status_code).to eq 200
      expect(page.response_headers['Content-Type']).to match /^application\/json/
    end
  end
end