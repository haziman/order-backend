require "rails_helper"

RSpec.feature "User Registration", :type => :feature do
  scenario "User have a way to register" do
    visit root_path
    
    expect(current_path).to eq new_user_session_path
    expect(page).to have_selector('form#new_user')
    expect { find_link("Sign up") }.not_to raise_error 
    
    click_link("Sign up")
    expect(current_path).to eq new_user_registration_path
  end
  
  scenario "Registration process (with valid/invalid info)" do
    def register!(mustfail = true)
      click_button("Sign up")
      expect(current_path).to eq root_path
      if mustfail
        expect(page).to have_css 'div#error_explanation' 
      else
        expect(page).not_to have_css 'div#error_explanation'
      end
    end
    
    visit new_user_registration_path
    register!

    fill_in 'user_firstname', with: "IG Test"
    fill_in 'user_lastname', with: "user"
    register!

    fill_in 'user_username', with: "igtest"
    register!

    fill_in 'user_email', with: "igtest@orderbackend.local"
    register!

    fill_in 'user_password', with: "testuser"
    fill_in 'user_password_confirmation', with: "testuse"
    register!

    fill_in 'user_password', with: "testuser"
    fill_in 'user_password_confirmation', with: "testuser"
    register!

    fill_in 'user_email', with: "igtest"
    fill_in 'user_password', with: "testuser"
    fill_in 'user_password_confirmation', with: "testuser"
    register!

    fill_in 'user_email', with: "igtest@orderbackend.local"
    fill_in 'user_password', with: "testuser"
    fill_in 'user_password_confirmation', with: "testuser"
    select('Administrator', :from => 'Role')
    register!(false)
  end
  
  
end