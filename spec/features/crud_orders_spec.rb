require 'spec_helper'

describe "Order CRUD" do
  
  def visit_put_api(data = {}, enable_token = true, method = :put)
    if enable_token
      page.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
    else
      page.driver.header('Authorization', '')
    end
    page.driver.submit method, "/api/v1/orders/#{data[:order][:id]}.json", data
  end
  
  def visit_post_api(data = {}, enable_token = true)
    if enable_token
      page.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
    else
      page.driver.header('Authorization', '')
    end
    page.driver.submit :post, "/api/v1/orders.json", data
  end
  
  describe "GET ng/views/orders/index" do
    it "renders the page" do
      visit ng_path("orders", "index")
      expect(page.status_code).to eq 200
      expect(page).to have_css('h1', text: I18n.t('orders$index.page_heading'))
    end
  end
  
  describe "GET api/v1/orders.json" do
    def visit_api
      page.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
      visit '/api/v1/orders.json'
    end
    
    it "should not be able to get data without access_token" do
      visit api_v1_orders_path
      expect(page.status_code).to eq 401
    end
    
    it "should be able to get data with access_token" do
      visit_api
      expect(page.status_code).to eq 200
    end
    
    it "should return JSON" do
      visit_api
      expect(page.response_headers['Content-Type']).to match /^application\/json/
    end
    
    it "should return JSON understandable by DataTables" do
      visit_api
      parsed = JSON.parse(page.body)
      expect(parsed).to include('draw')
      expect(parsed).to include('data')
    end
  end
  
  describe "POST /api/v1/orders.json" do
    it "should not be able to post data without access_token" do
      visit_post_api({order: {id: 1}}, false)
      expect(page.status_code).to eq 401
    end
    
    it "should not be able to post invalid data with access_token" do
      visit_post_api({
        order: { 
          product_id: 1
        }
      })
      expect(page.status_code).to eq 500
    end
    
    it "should be able to post data with access_token and receive JSON" do
      visit_post_api({
        order: { 
          product_id: 1,
          user_id:1,
          orderstatus_id: 1
        }
      })
      expect(page.status_code).to eq 200
      expect(page.response_headers['Content-Type']).to match /^application\/json/
      parsed = JSON.parse(page.body)
      expect(parsed["status"]["code"]).to eq "success"
    end
  end
  
  describe "PUT /api/v1/orders.json" do
    
    it "should not be able to update user without access_token" do
      visit_put_api({order: {id: 1}}, false)
      expect(page.status_code).to eq 401
    end
    
    it "should not be able to update invalid data with access_token" do
      visit_post_api({
        order: { 
          product_id: 1,
          user_id:1,
          orderstatus_id: 1
        }
      })
      expect(page.status_code).to eq 200
      visit_put_api({
        order: { 
          id: 1,
          user_id: nil
        }
      })
      expect(page.status_code).to eq 500
    end
    
    it "should be able to update data with access_token and receive JSON" do
      visit_post_api({
        order: { 
          product_id: 1,
          user_id:1,
          orderstatus_id: 1
        }
      })
      expect(page.status_code).to eq 200
      visit_put_api({
        order: { 
          id: 1,
          orderstatus_id: 2
        }
      })
      expect(page.status_code).to eq 200
      expect(page.response_headers['Content-Type']).to match /^application\/json/
    end
  end
end