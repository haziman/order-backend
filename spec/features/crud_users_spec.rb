require 'spec_helper'

describe "User CRUD" do
  
  def visit_put_api(data = {}, enable_token = true, method = :put)
    if enable_token
      page.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
    else
      page.driver.header('Authorization', '')
    end
    page.driver.submit method, "/api/v1/users/#{data[:user][:id]}.json", data
  end
  
  def visit_post_api(data = {}, enable_token = true)
    if enable_token
      page.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
    else
      page.driver.header('Authorization', '')
    end
    page.driver.submit :post, "/api/v1/users.json", data
  end
  
  describe "GET ng/views/users/index" do
    it "renders the page" do
      visit ng_path("users", "index")
      expect(page.status_code).to eq 200
      expect(page).to have_css('h1', text: I18n.t('users$index.page_heading'))
    end
  end
  
  describe "GET api/v1/users.json" do
    def visit_api
      page.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
      visit '/api/v1/users.json'
    end
    
    it "should not be able to get data without access_token" do
      visit api_v1_users_path
      expect(page.status_code).to eq 401
    end
    
    it "should be able to get data with access_token" do
      visit_api
      expect(page.status_code).to eq 200
    end
    
    it "should return JSON" do
      visit_api
      expect(page.response_headers['Content-Type']).to match /^application\/json/
    end
    
    it "should return JSON understandable by DataTables" do
      visit_api
      parsed = JSON.parse(page.body)
      expect(parsed).to include('draw')
      expect(parsed).to include('data')
    end
  end
  
#  I didn't expose this because I can get the data from DataTables
#  describe "GET api/v1/users/:id.json" do
#    def visit_api
#      Capybara.current_session.driver.header('Authorization', 'Bearer '.concat(@accesstoken))
#      visit '/api/v1/users/1.json'
#    end
#    
#    it "should not be able to get data without access_token" do
#      visit api_v1_users_path(1)
#      expect(page.status_code).to eq 401
#    end
#    
#    it "should be able to get data with access_token" do
#      visit_api
#      expect(page.status_code).to eq 200
#    end
#    
#    it "should return JSON" do
#      visit_api
#      expect(page.response_headers['Content-Type']).to match /^application\/json/
#    end
#  end
  
  describe "POST /api/v1/users.json" do
    it "should not be able to post data without access_token" do
      visit_post_api({user: {id: 1}}, false)
      expect(page.status_code).to eq 401
    end
    
    it "should not be able to post invalid data with access_token" do
      visit_post_api({
        user: { 
          firstname: "user_post", lastname: "test", 
          password: "userpost", password_confirmation: "userpost"
        }
      })
      expect(page.status_code).to eq 500
    end
    
    it "should be able to post data with access_token and receive JSON" do
      visit_post_api({
        user: { 
          firstname: "user_post", lastname: "test", username: "userpost", 
          email: "userpost@orderbackend.local", role_id: 1, password: "userpost", 
          password_confirmation: "userpost"
        }
      })
      expect(page.status_code).to eq 200
      expect(page.response_headers['Content-Type']).to match /^application\/json/
      parsed = JSON.parse(page.body)
      expect(parsed["status"]["code"]).to eq "success"
    end
  end
  
  describe "PUT /api/v1/users.json" do
    
    it "should not be able to update user without access_token" do
      visit_put_api({user: {id: 1}}, false)
      expect(page.status_code).to eq 401
    end
    
    it "should not be able to update invalid data with access_token" do
      visit_post_api({
        user: { 
          firstname: "user_post", lastname: "test", username: "userpost", 
          email: "userpost@orderbackend.local", role_id: 1, password: "userpost", 
          password_confirmation: "userpost"
        }
      })
      expect(page.status_code).to eq 200
      visit_put_api({
        user: { 
          id: 2,
          email: "testing@orderingbackend.local"
        }
      })
      expect(page.status_code).to eq 500
    end
    
    it "should be able to update data with access_token and receive JSON" do
      visit_post_api({
        user: {
          firstname: "user_post", lastname: "test", username: "userpost", 
          email: "userpost@orderbackend.local", role_id: 1, password: "userpost", 
          password_confirmation: "userpost"
        }
      })
      expect(page.status_code).to eq 200
      visit_put_api({
        user: {
          id: 2,
          role_id: 2
        }
      })
      expect(page.status_code).to eq 200
      expect(page.response_headers['Content-Type']).to match /^application\/json/
    end
  end
end