require 'spec_helper'

describe "Mailer - Order Mailer" do
  include ActiveJob::TestHelper
  
  subject(:trigger_job) { order.process and OrderMailer.notify_user_processed(order).deliver_later! }
  let(:order){ Order.create(user_id: 1, product_id: 1, orderstatus_id: 1) }
  
  it "mailer job is created" do
    expect { trigger_job }
      .to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end
  
  it 'an email actually go through' do
    expect {
        perform_enqueued_jobs { trigger_job }
    }.to change { ActionMailer::Base.deliveries.size }.by(1)
  end
  
  it 'notification email is sent to the right user' do
    perform_enqueued_jobs { trigger_job }

    mail = ActionMailer::Base.deliveries.last
    expect(mail.to[0]).to eq order.user.email
  end
end