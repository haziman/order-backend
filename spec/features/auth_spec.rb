require "rails_helper"

RSpec.feature "User authentication", :type => :feature do
  scenario "User presented with login page" do
    visit root_path
    
    expect(current_path).to eq new_user_session_path
    expect(page).to have_selector('form#new_user')
  end
  
  scenario "User must login with correct credentials" do
    visit new_user_session_path
    
    fill_in 'user_login', with: "unknown"
    fill_in 'user_password', with: "user"
    
    click_button('Log in')
    
    expect(page).to have_css 'p.flash.alert'
    expect(current_path).not_to eq root_path
  end
  
  scenario "User can login with correct credential" do
    visit new_user_session_path
    
    fill_in 'user_login', with: "testuser"
    fill_in 'user_password', with: "testuser"
    
    click_button('Log in')
    
    expect(current_path).to eq root_path
  end
end