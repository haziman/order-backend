require 'spec_helper'

describe "Job - Order Processor" do
  include ActiveJob::TestHelper
  
  before do
    @order = Order.create({ user_id: 1, product_id: 1, orderstatus_id: 1 })
  end

  subject(:job) { 
    OrderProcessorJob.set( wait: 5.seconds ).perform_later @order
  }
  
  it 'queues the job' do
    expect { job }
      .to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end
  
  it 'is in default queue' do
    expect(OrderProcessorJob.new.queue_name).to eq('default')
  end
  
  it 'executes perform' do
    perform_enqueued_jobs { 
      expect_any_instance_of(Order).to receive(:process)
      job 
    }
  end
end